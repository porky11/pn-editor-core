use crate::{path_map::PathMap, text_content::TextContent, vector::Vector};

use pns::{Pid, Tid};
use text_editing::TextLine;

use std::collections::BTreeMap;

pub struct TransitionNodeMap {
    pub nodes: BTreeMap<Tid, TransitionNode>,
    pub paths: PathMap<Tid>,
}

impl TransitionNodeMap {
    pub fn new() -> Self {
        Self {
            nodes: BTreeMap::new(),
            paths: PathMap::new(),
        }
    }

    pub fn from_map(nodes: BTreeMap<Tid, TransitionNode>) -> Self {
        let mut paths = PathMap::new();
        for (&tid, node) in &nodes {
            let path = node.path.clone();
            paths.insert(path, tid);
        }
        Self { nodes, paths }
    }

    pub fn node(&self, tid: Tid) -> &TransitionNode {
        &self.nodes[&tid]
    }

    pub fn node_mut(&mut self, tid: Tid) -> &mut TransitionNode {
        unsafe { self.nodes.get_mut(&tid).unwrap_unchecked() }
    }

    pub fn insert(&mut self, tid: Tid, node: TransitionNode) {
        let path = node.path.clone();
        self.nodes.insert(tid, node);
        self.paths.insert(path, tid);
    }

    pub fn remove(&mut self, tid: Tid) -> Option<TransitionNode> {
        self.nodes.remove(&tid).inspect(|node| {
            self.paths.remove(&node.path, &tid);
        })
    }
}

pub struct PlaceNode {
    pub positions: Vec<Vector>,
    pub name: TextLine,
    pub name_cursor: usize,
}

impl PlaceNode {
    pub fn new(positions: &[Vector], name: String) -> Self {
        let name = TextLine::from_string(name.chars().collect());
        let name_cursor = name.len();
        Self {
            positions: positions.to_vec(),
            name,
            name_cursor,
        }
    }
}

pub struct TransitionNode {
    pub position: Vector,
    pub path: Vec<Box<str>>,
    pub name: TextLine,
    pub name_cursor: usize,
    pub content: TextContent,
}

impl TransitionNode {
    pub fn new(position: Vector, path: Vec<Box<str>>, name: String) -> Self {
        let name = TextLine::from_string(name);
        let name_cursor = name.len();
        Self {
            position,
            path,
            name,
            name_cursor,
            content: TextContent::new(vec![TextLine::new()]),
        }
    }
}

pub struct NodeContainer {
    pub places: BTreeMap<Pid, PlaceNode>,
    pub transitions: TransitionNodeMap,
}

impl NodeContainer {
    pub fn empty() -> Self {
        Self {
            places: BTreeMap::new(),
            transitions: TransitionNodeMap::new(),
        }
    }

    pub fn new(
        places: BTreeMap<Pid, PlaceNode>,
        transitions: BTreeMap<Tid, TransitionNode>,
    ) -> Self {
        Self {
            places,
            transitions: TransitionNodeMap::from_map(transitions),
        }
    }
}
