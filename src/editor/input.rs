mod copypaste;
mod files;
mod grab;
mod text;

use super::Editor;

use crate::{
    edit_list::{EditList, NodeInstanceId, SelectionList},
    feature_types::{SearchId, StringSearcher, ViewMode},
    node::{NodeContainer, PlaceNode, TransitionNode},
    state_info::{CallableState, StateInfo},
    vector::Vector,
};

use bool_utils::BoolUtils;
use dialog::{DialogBox, Input};
use event_simulation::DynamicSimulation;
use petri_net_simulation::{DynamicNet, DynamicNetEdit, MultiPetriNetSimulation};
use pns::{Net, Pid, Tid};
use rfd::{MessageButtons, MessageDialog, MessageDialogResult, MessageLevel};
use text_editing::TextLine;

use std::collections::{BTreeMap, btree_map::Entry};

fn snap(pos: &mut Vector, snap_size: f32) {
    pos.x = (pos.x / snap_size).round() * snap_size;
    pos.y = (pos.y / snap_size).round() * snap_size;
}

impl Editor {
    fn next_transition_name(&self) -> String {
        let count = self.nodes.transitions.nodes.len();

        if count > 0 {
            format!("Scene {count}")
        } else {
            "Intro".to_string()
        }
    }

    fn reset(&mut self, mut net: Net, paths: Vec<Vec<Box<str>>>, place_names: Vec<String>) {
        for _ in (net.place_count())..place_names.len() {
            net.add_place();
        }

        for _ in (net.transition_count())..paths.len() {
            net.add_transition();
        }

        self.nodes = NodeContainer::new(BTreeMap::new(), BTreeMap::new());

        let mut place_names = place_names.into_iter();
        let place_count = net.place_count();
        for (i, pid) in net.place_ids().enumerate() {
            let name = place_names.next().unwrap_or_default();
            let node = PlaceNode::new(
                &[Vector::new(
                    self.node_settings.width,
                    self.node_settings.height * (i as i32 * 2 - place_count as i32) as f32,
                )],
                name,
            );
            self.nodes.places.insert(pid, node);
        }

        let mut names = paths.into_iter();
        let transition_count = net.transition_count();
        for (i, tid) in net.transition_ids().enumerate() {
            let mut path = names.next().unwrap_or_default();
            let name = path
                .pop()
                .map_or_else(|| self.next_transition_name(), Into::into);
            let node = TransitionNode::new(
                Vector::new(
                    -self.node_settings.width,
                    self.node_settings.height * (i as i32 * 2 - transition_count as i32) as f32,
                ),
                path,
                name,
            );
            self.nodes.transitions.insert(tid, node);
        }

        self.net = DynamicNet::Default(net.into());

        self.state_infos = Vec::new();

        self.selected = SelectionList::new();
        self.view_offset = Vector::new(0.0, 0.0);

        self.simulation = 0;
        self.changes = false;
    }

    fn line_count(&self) -> usize {
        ((self.window_size.y / self.node_settings.height * 2.0) as usize).saturating_sub(5)
    }

    fn add_transition(
        &mut self,
        tid: Tid,
        position: Vector,
        path: Vec<Box<str>>,
        name: Option<String>,
    ) -> NodeInstanceId {
        self.changes = true;
        let new_node = TransitionNode::new(
            position,
            path,
            name.map_or_else(|| self.next_transition_name(), |name| name),
        );
        self.nodes.transitions.insert(tid, new_node);
        let DynamicNet::Simulated(net) = &mut self.net else {
            return NodeInstanceId::Transition(tid);
        };

        for (info, state) in self.state_infos.iter_mut().zip(net.states_mut()) {
            if let Entry::Vacant(entry) = info.callables.entry(tid) {
                entry.insert(CallableState::default());
                info.update_state(state);
            }
        }

        NodeInstanceId::Transition(tid)
    }

    fn add_place(&mut self, pid: Pid, position: Vector, name: String) -> NodeInstanceId {
        self.changes = true;
        let new_node = PlaceNode::new(&[position], name);
        self.nodes.places.insert(pid, new_node);
        NodeInstanceId::Place(pid, 0)
    }

    fn add_linked_place(
        &mut self,
        pid: Pid,
        positions: &[Vector],
        name: String,
    ) -> impl Iterator<Item = NodeInstanceId> + use<> {
        self.changes = true;
        let new_node = PlaceNode::new(positions, name);
        self.nodes.places.insert(pid, new_node);
        (0..positions.len()).map(move |subindex| NodeInstanceId::Place(pid, subindex))
    }

    /// Switches simulation mode.
    pub fn switch_mode(&mut self, ignore_save_warning: bool) {
        use DynamicSimulation::*;
        if ignore_save_warning || !self.ensure_saved(true) {
            return;
        }
        replace_with::replace_with_or_abort(&mut self.net, |old_net| match old_net {
            Default(net) => Simulated(MultiPetriNetSimulation::new(net)),
            Simulated(net) => Default(net.release()),
        });

        let Simulated(net) = &mut self.net else {
            self.state_infos = Vec::new();
            return;
        };

        net.add_simulation();
        let mut info = StateInfo {
            state_path: None,
            changes: false,
            callables: net
                .transition_ids()
                .map(|tid| (tid, CallableState::default()))
                .collect(),
        };
        self.simulation = 0;
        info.update_state(&mut net.states_mut()[0]);
        self.state_infos.push(info);
    }

    fn switch_simulation(&mut self) {
        let Some(net) = self.net.simulated() else {
            return;
        };

        self.simulation += 1;
        if self.simulation == net.states().len() {
            self.simulation = 0;
        }
    }

    // edit

    /// Adds a token to nodes if they are places.
    pub fn start_nodes(&mut self) {
        self.changes = true;
        for &pid in self.selected.pids.keys() {
            DynamicNetEdit::new(&mut self.net).add_initial_tokens(pid, 1);

            let Some(net) = self.net.simulated_mut() else {
                continue;
            };

            for (info, state) in self.state_infos.iter_mut().zip(net.states_mut()) {
                info.update_state(state);
            }
        }
    }

    /// Removes all selected nodes.
    pub fn remove_nodes(&mut self) {
        self.changes = true;

        if self.view_mode != ViewMode::Actions {
            let mut removed = false;
            let mut net = DynamicNetEdit::new(&mut self.net);
            for (&pid, subindices) in self.selected.pids.iter() {
                let positions =
                    &mut unsafe { self.nodes.places.get_mut(&pid).unwrap_unchecked() }.positions;
                for index in subindices.iter().rev() {
                    positions.remove(*index);
                }
                if positions.is_empty() {
                    removed = true;
                    net.remove_place(pid);
                    self.nodes.places.remove(&pid);
                }
            }
            drop(net);

            if removed {
                if let Some(net) = self.net.simulated_mut() {
                    for (info, state) in self.state_infos.iter_mut().zip(net.states_mut()) {
                        info.update_state(state);
                    }
                }
            }
        }

        if self.view_mode != ViewMode::State {
            if let Some(net) = self.net.default_mut() {
                for &tid in &self.selected.tids {
                    net.remove_transition(tid);
                    self.nodes.transitions.remove(tid);
                }
            }
        }

        self.selected.clear();
    }

    fn common_path_length(&self) -> Option<usize> {
        let mut tids = self.selected.tids.iter();
        let &tid = tids.next()?;
        let transition = self.nodes.transitions.node(tid);
        let mut main_path = transition.path.clone();

        for &tid in tids {
            if main_path.is_empty() {
                break;
            }
            let transition = self.nodes.transitions.node(tid);
            let mut main_entries = main_path.iter();
            let mut remove_more = false;
            for entry in &transition.path {
                let Some(next_entry) = main_entries.next() else {
                    break;
                };

                if entry == next_entry {
                    continue;
                }

                remove_more = true;
                break;
            }

            let remove = main_entries.len();

            for _ in 0..remove {
                main_path.pop();
            }
            if remove_more {
                main_path.pop();
            }
        }

        Some(main_path.len())
    }

    /// Group all selected nodes.
    pub fn group_nodes(&mut self, keep: bool) {
        if self.view_mode == ViewMode::State {
            return;
        }
        let Ok(Some(group_name)) = Input::new("Group name:").show() else {
            return;
        };
        if keep {
            let Some(len) = self.common_path_length() else {
                return;
            };

            for &tid in &self.selected.tids {
                let Some(mut transition) = self.nodes.transitions.remove(tid) else {
                    continue;
                };

                transition
                    .path
                    .insert(len, group_name.clone().into_boxed_str());
                self.nodes.transitions.insert(tid, transition);
            }
        } else {
            for &tid in &self.selected.tids {
                let Some(mut transition) = self.nodes.transitions.remove(tid) else {
                    continue;
                };

                transition.path = vec![group_name.clone().into_boxed_str()];
                self.nodes.transitions.insert(tid, transition);
            }
        }

        self.changes = true;
    }

    /// Remove all selected nodes from group.
    pub fn ungroup_nodes(&mut self, all: bool) {
        if self.view_mode == ViewMode::State {
            return;
        }
        let Some(len) = self.common_path_length() else {
            return;
        };
        if len == 0 {
            return;
        }
        let len = len - 1;

        for &tid in &self.selected.tids {
            let Some(mut transition) = self.nodes.transitions.remove(tid) else {
                continue;
            };

            if all {
                transition.path = transition.path[len..].into();
            } else {
                transition.path.remove(len);
            }

            self.nodes.transitions.insert(tid, transition);
        }

        self.changes = true;
    }

    /// Should be called when the window is resized.
    pub fn resize(&mut self, width: u32, height: u32) {
        self.window_size = Vector::new(width as f32, height as f32);
    }

    /// Cycle through states.
    pub fn cycle(&mut self, skip: bool) {
        let Some(string_searcher) = &mut self.string_searcher else {
            self.switch_simulation();
            return;
        };

        let Some(index) = &mut string_searcher.index else {
            return;
        };

        use SearchId::*;
        match index {
            Place(pindex) => {
                if skip {
                    if !string_searcher.list.tids.is_empty() {
                        *index = Transition(0);
                    }

                    return;
                }

                *pindex += 1;
                if *pindex < string_searcher.list.pids.len() {
                    return;
                }

                *index = if string_searcher.list.tids.is_empty() {
                    Place(0)
                } else {
                    Transition(0)
                }
            }
            Transition(tindex) => {
                if skip {
                    if !string_searcher.list.pids.is_empty() {
                        *index = Place(0);
                    }

                    return;
                }

                *tindex += 1;
                if *tindex < string_searcher.list.tids.len() {
                    return;
                }

                *index = if string_searcher.list.pids.is_empty() {
                    Transition(0)
                } else {
                    Place(0)
                }
            }
        }
    }

    /// Adds a new simulation state.
    pub fn add_state(&mut self) {
        let DynamicNet::Simulated(net) = &mut self.net else {
            return;
        };

        net.add_simulation();
        let new_index = net.states().len() - 1;
        let mut info = StateInfo {
            state_path: None,
            changes: false,
            callables: net
                .transition_ids()
                .map(|tid| (tid, CallableState::default()))
                .collect(),
        };
        self.simulation = new_index;
        info.update_state(&mut net.states_mut()[new_index]);
        self.state_infos.push(info);
    }

    /// Toggles snapping.
    pub fn toggle_snapping(&mut self) {
        self.snapping = if self.snapping.is_some() {
            None
        } else {
            Some(16.0)
        };
    }

    /// Toggles the editor.
    pub fn toggle_editor(&mut self) {
        self.text_content_shown.toggle();
        self.string_searcher = None;
    }

    /// Enables the search bar.
    pub fn start_search_mode(&mut self) {
        if self.string_searcher.is_some() {
            return;
        }

        self.text_content_shown = false;
        self.string_searcher = Some(StringSearcher {
            name: TextLine::new(),
            name_cursor: 0,
            index: None,
            list: EditList::new(),
        });
    }

    /// Loads in an empty net.
    pub fn load_new(&mut self, ignore_save_warning: bool) {
        if ignore_save_warning || self.ensure_saved(false) {
            self.net_path = None;
            self.reset(Net::new(), Vec::new(), Vec::new());
        }
    }

    /// Changes the view mode.
    /// See `ViewMode` for more information about the view modes.
    pub fn set_view_mode(&mut self, view_mode: ViewMode) {
        self.view_mode = view_mode;
    }

    /// Zoom in or out by a factor.
    pub fn zoom(&mut self, mut factor: f32, zoom_scale: bool, zoom_distance: bool) {
        self.view_offset = self.relative_mouse_pos();
        if zoom_scale {
            self.zoom *= factor;
        } else {
            factor = 1.0 / factor;
        }

        if zoom_distance || !zoom_scale {
            fn zoom_position(position: &mut Vector, view_offset: Vector, factor: f32) {
                *position = *position * factor + view_offset * (1.0 - factor);
            }

            for node in self.nodes.transitions.nodes.values_mut() {
                zoom_position(&mut node.position, self.view_offset, factor);
            }

            for node in self.nodes.places.values_mut() {
                for position in &mut node.positions {
                    zoom_position(position, self.view_offset, factor);
                }
            }
        }
        self.view_offset = self.view_offset * 2.0 - self.relative_mouse_pos();
    }

    /// Ensure the net be saved.
    pub fn ensure_saved(&self, state_only: bool) -> bool {
        if (state_only || !self.changes) && !self.state_infos.iter().any(|info| info.changes) {
            return true;
        }

        MessageDialog::new()
            .set_level(MessageLevel::Warning)
            .set_title("Warning!")
            .set_description("There are unsaved changes, that will be lost\nAre you sure?")
            .set_buttons(MessageButtons::YesNo)
            .show()
            == MessageDialogResult::Yes
    }
}
