mod helpers;
mod position_info;

use super::{Editor, snap};

use crate::{
    ActionButton, FireDirection,
    edit_list::{NodeInstanceId, SelectionList},
    feature_types::{DuplicationType, Grab, GrabKind, MoveType, ViewMode},
    vector::Vector,
};

use event_simulation::{CallState, PlayDirection, Simulation};
use petri_net_simulation::DynamicNetEdit;
use pns::{Pid, Tid};

use std::collections::HashMap;

impl Editor {
    /// Grab something.
    ///
    /// * `button`: specifies which action button is used.
    /// * `clear_old`: specifies if old selections should be deselected.
    /// * `create`: creates new nodes.
    pub fn grab(&mut self, button: ActionButton, clear_old: bool, create: bool) {
        if let Some(node_id) = self.click_info(self.relative_mouse_pos()) {
            self.grab_node(node_id, button, clear_old, create);
        } else {
            self.grab_empty(button, clear_old, create);
        }
    }

    fn snap_on_release(&mut self, kind: GrabKind) {
        let Some(snap_size) = self.snapping else {
            return;
        };

        let GrabKind::Move(move_type) = kind else {
            return;
        };

        match move_type {
            MoveType::Nodes => {
                for &tid in &self.selected.tids {
                    let node = self.nodes.transitions.node_mut(tid);
                    snap(&mut node.position, snap_size);
                }
                for (pid, subindices) in self.selected.pids.iter() {
                    let node = unsafe { self.nodes.places.get_mut(pid).unwrap_unchecked() };
                    for &index in subindices {
                        snap(&mut node.positions[index], snap_size);
                    }
                }
            }
            MoveType::View => snap(&mut self.view_offset, snap_size),
        }
    }

    /// Release something.
    pub fn release(
        &mut self,
        button: ActionButton,
        allow_more: bool,
        clear_old: bool,
        select_new: bool,
    ) {
        let Some(Grab {
            kind,
            moved,
            deselect,
            ..
        }) = self.grab
        else {
            return;
        };

        if button == ActionButton::Special {
            match kind {
                GrabKind::Select => {
                    let node_ids = self.box_info();
                    if clear_old && select_new {
                        self.selected.clear();
                    }
                    for node_id in node_ids {
                        if !select_new {
                            self.selected.remove_node(node_id);
                        } else if !self.selected.contains(&node_id) {
                            self.selected.add(node_id);
                        }
                    }
                }
                GrabKind::Connect => self.release_connect(allow_more, clear_old, select_new),
                GrabKind::Move(_) => (),
            }
        }

        if let (false, GrabKind::Move(_)) = (moved, kind) {
            let click_pos = self.relative_mouse_pos();
            if let Some(node_id) = self.click_info(click_pos) {
                if clear_old {
                    self.selected.clear();
                    self.selected.add(node_id);
                } else if deselect {
                    self.selected.remove_node(node_id);
                }
            } else if clear_old {
                self.selected.clear();
            }
        }

        self.snap_on_release(kind);

        self.grab = None;
    }

    /// Set cursor position.
    pub fn set_cursor(&mut self, x: f32, y: f32) {
        self.mouse_pos = Vector::new(x, y);

        let Some(Grab {
            moved, duplicate, ..
        }) = &mut self.grab
        else {
            return;
        };

        if *moved {
            return;
        }

        *moved = true;

        let Some(duplication_type) = duplicate else {
            return;
        };

        let linked = *duplication_type == DuplicationType::Linked;

        struct TransitionInfo {
            path: Vec<Box<str>>,
            name: String,
            tid: Tid,
            position: Vector,
        }

        struct PlaceInfo {
            name: String,
            pid: Pid,
            positions: Vec<Vector>,
        }

        let mut transition_infos: Vec<TransitionInfo> = Vec::new();
        let mut place_infos: Vec<PlaceInfo> = Vec::new();

        match self.view_mode {
            ViewMode::Default => {
                let pids = &self.selected.pids;
                let tids = &self.selected.tids;

                let mut place_mappings = HashMap::new();
                let mut connecting_tids = Vec::new();
                for (&pid, subindices) in pids.iter() {
                    let new_pid = if linked {
                        pid
                    } else {
                        let place = &self.net.place(pid);
                        let mut duplicate = true;
                        for &tid in place.prev().iter().chain(place.next()) {
                            if tids.contains(&tid) {
                                duplicate = false;
                            } else {
                                connecting_tids.push(tid);
                            }
                        }

                        let mut net = DynamicNetEdit::new(&mut self.net);
                        if duplicate {
                            net.duplicate_place(pid)
                        } else {
                            let index = net.add_place();
                            let count = { net.initial_token_count(pid) };
                            net.add_initial_tokens(index, count);
                            place_mappings.insert(pid, index);
                            index
                        }
                    };
                    let node = &self.nodes.places[&pid];
                    place_infos.push(PlaceInfo {
                        name: node.name.to_string(),
                        pid: new_pid,
                        positions: subindices
                            .iter()
                            .map(|&subindex| node.positions[subindex])
                            .collect(),
                    });
                }
                let mut net = DynamicNetEdit::new(&mut self.net);
                for &tid in tids {
                    let node = &self.nodes.transitions.node(tid);
                    let transition = &net.transition(tid);
                    let mut prev_pids = Vec::new();
                    let mut next_pids = Vec::new();
                    for &pid in transition.prev() {
                        let mapped_pid = *place_mappings.get(&pid).unwrap_or(&pid);
                        prev_pids.push(mapped_pid);
                    }
                    for &pid in transition.next() {
                        let mapped_pid = *place_mappings.get(&pid).unwrap_or(&pid);
                        next_pids.push(mapped_pid);
                    }
                    let index = net.add_connected_transition(&prev_pids, &next_pids);

                    transition_infos.push(TransitionInfo {
                        path: node.path.clone(),
                        name: node.name.to_string(),
                        tid: index,
                        position: node.position,
                    });
                }
                if !linked {
                    for tid in connecting_tids {
                        let transition = &net.transition(tid);
                        let mut prev_pids = Vec::new();
                        let mut next_pids = Vec::new();
                        for &pid in transition.prev() {
                            let &mapped_pid = place_mappings.get(&pid).unwrap_or(&pid);
                            prev_pids.push(mapped_pid);
                        }
                        for &pid in transition.next() {
                            let &mapped_pid = place_mappings.get(&pid).unwrap_or(&pid);
                            next_pids.push(mapped_pid);
                        }
                        for pid in prev_pids {
                            net.connect_place_to_transition(pid, tid);
                        }
                        for pid in next_pids {
                            net.connect_transition_to_place(tid, pid);
                        }
                    }
                }
            }
            ViewMode::State => {
                let mut net = DynamicNetEdit::new(&mut self.net);
                for (&pid, subindices) in self.selected.pids.iter() {
                    let new_pid = if linked {
                        pid
                    } else {
                        net.duplicate_place(pid)
                    };
                    let node = &self.nodes.places[&pid];
                    place_infos.push(PlaceInfo {
                        name: node.name.to_string(),
                        pid: new_pid,
                        positions: subindices
                            .iter()
                            .map(|&subindex| node.positions[subindex])
                            .collect(),
                    });
                }
            }
            ViewMode::Actions => {
                let mut net = DynamicNetEdit::new(&mut self.net);
                for &tid in &self.selected.tids {
                    let node = &self.nodes.transitions.node(tid);
                    let new_tid = net.duplicate_transition(tid);
                    transition_infos.push(TransitionInfo {
                        path: node.path.clone(),
                        name: node.name.to_string(),
                        tid: new_tid,
                        position: node.position,
                    });
                }
            }
        }

        let mut new_nodes = Vec::new();

        if self.view_mode != ViewMode::State {
            for TransitionInfo {
                path,
                name,
                tid,
                position,
            } in transition_infos
            {
                new_nodes.push(self.add_transition(tid, position, path, Some(name)));
            }
        }

        if self.view_mode != ViewMode::Actions {
            for PlaceInfo {
                name,
                pid,
                positions,
            } in place_infos
            {
                if linked {
                    new_nodes.extend(self.extend_place_links(pid, &positions));
                } else {
                    new_nodes.extend(self.add_linked_place(pid, &positions, name));
                };
            }
        }

        let mut new_edit = SelectionList::new();
        for node in new_nodes {
            new_edit.add(node);
        }
        self.selected = new_edit;
    }

    /// Fire node.
    pub fn fire_node(&mut self, direction: FireDirection) {
        let Some(node_id) = self.click_info(self.relative_mouse_pos()) else {
            return;
        };

        let Some(net) = self.net.simulated_mut() else {
            return;
        };

        let NodeInstanceId::Transition(tid) = node_id else {
            return;
        };

        let mut state = unsafe { net.get_mut(self.simulation).unwrap_unchecked() };

        fn handle_fire<D: PlayDirection>(
            fire: CallState<impl Simulation<Event = Tid>, D>,
            tid: Tid,
        ) -> bool {
            for (i, fire_tid) in fire.callables.iter().enumerate() {
                if *fire_tid == tid {
                    fire.call(i);
                    return true;
                }
            }

            false
        }

        let changed = match direction {
            FireDirection::Forward => handle_fire(state.prepare_call(), tid),
            FireDirection::Backward => handle_fire(state.prepare_revert(), tid),
        };

        if changed {
            let info = &mut self.state_infos[self.simulation];
            info.changes = true;
            info.update_state(state.state);
        }
    }
}
