use super::Editor;

use crate::{text_content::TextContent, vector::Vector};

use header_parsing::parse_header;
use pns::Net;
use text_editing::TextLine;

use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader, Error as IOError},
    path::{Path, PathBuf},
};

pub enum Error {
    IO(IOError),
    NoDirectorySpecified,
    SubheaderWithoutHeader(usize),
}

impl From<IOError> for Error {
    fn from(err: IOError) -> Self {
        Self::IO(err)
    }
}

fn load_keys(path: &Path) -> Result<Vec<String>, IOError> {
    let Ok(file) = File::open(path) else {
        return Ok(Vec::new());
    };

    let mut result = Vec::new();
    for line in BufReader::new(file).lines() {
        result.push(line?);
    }

    Ok(result)
}

struct SortedTextMap {
    names: Vec<Vec<Box<str>>>,
    contents: HashMap<Vec<Box<str>>, Vec<TextLine>>,
}

impl SortedTextMap {
    fn new(names: Vec<Vec<Box<str>>>) -> Self {
        Self {
            names,
            contents: HashMap::new(),
        }
    }

    fn add_content(&mut self, key: &Vec<Box<str>>, content: Vec<TextLine>) {
        if self.names.contains(key) {
            self.contents.insert(key.clone(), content);
        }
    }
}

impl Editor {
    fn load_layout(&mut self, file: &mut File) -> Result<(), Error> {
        let mut removed_tids = Vec::new();
        for (&tid, node) in &mut self.nodes.transitions.nodes {
            use std::io::Read;
            let mut buffer: [u8; 8] = unsafe { std::mem::zeroed() };
            file.read_exact(&mut buffer)?;
            if 0x7FFFFFFF == u64::from_le_bytes(buffer) {
                removed_tids.push(tid);
            } else {
                node.position = Vector::new(
                    f32::from_le_bytes([buffer[0], buffer[1], buffer[2], buffer[3]]),
                    f32::from_le_bytes([buffer[4], buffer[5], buffer[6], buffer[7]]),
                );
            }
        }

        let mut removed_pids = Vec::new();
        for (&pid, node) in &mut self.nodes.places {
            use std::io::Read;
            node.positions.clear();
            let mut buffer: [u8; 8] = unsafe { std::mem::zeroed() };
            file.read_exact(&mut buffer)?;
            if 0x7FFFFFFF != u64::from_le_bytes(buffer) {
                let (first, count) = (
                    f32::from_le_bytes([buffer[0], buffer[1], buffer[2], buffer[3]]),
                    u32::from_le_bytes([buffer[4], buffer[5], buffer[6], buffer[7]]),
                );

                if first.is_nan() {
                    for _i in 0..count {
                        file.read_exact(&mut buffer)?;
                        node.positions.push(Vector::new(
                            f32::from_le_bytes([buffer[0], buffer[1], buffer[2], buffer[3]]),
                            f32::from_le_bytes([buffer[4], buffer[5], buffer[6], buffer[7]]),
                        ));
                    }
                } else {
                    node.positions.push(Vector::new(
                        f32::from_le_bytes([buffer[0], buffer[1], buffer[2], buffer[3]]),
                        f32::from_le_bytes([buffer[4], buffer[5], buffer[6], buffer[7]]),
                    ));
                }
            }
            if node.positions.is_empty() {
                removed_pids.push(pid);
            }
        }

        for tid in removed_tids {
            self.nodes.transitions.remove(tid);
        }
        for pid in removed_pids {
            self.nodes.places.remove(&pid);
        }

        Ok(())
    }

    fn load_story(file: &File, text_map: &mut SortedTextMap) -> Result<(), Error> {
        let mut first = false;
        let mut empty_count = 0;
        let mut current_key: Vec<Box<str>> = Vec::new();
        let mut current_content = Vec::new();

        for (line_number, line) in BufReader::new(file).lines().enumerate() {
            let line = line?;

            if let Some(success) = parse_header(&mut current_key, &line) {
                let Ok(changes) = success else {
                    return Err(Error::SubheaderWithoutHeader(line_number));
                };

                first = true;
                if !changes.path.is_empty() {
                    text_map.add_content(changes.path, current_content);
                    current_content = Vec::new();
                    empty_count = 0;
                }

                changes.apply();

                continue;
            }

            if line.is_empty() {
                if first {
                    first = false;
                } else {
                    empty_count += 1;
                }

                continue;
            }

            if !current_key.is_empty() {
                current_content.extend((0..empty_count).map(|_| TextLine::new()));
                empty_count = 0;
                current_content.push(TextLine::from_string(line));
            }
        }

        if !current_key.is_empty() {
            text_map.add_content(&current_key, current_content);
        }

        Ok(())
    }

    /// Loads the net from a specified path.
    pub fn load_net(&mut self, folder_path: PathBuf) -> Result<(), Error> {
        let Some((pns, pnl, pnk, pnkp, story)) = Self::project_paths(&folder_path) else {
            return Err(Error::NoDirectorySpecified);
        };

        let net = Net::load(pns.as_ref()).unwrap_or_default();

        let names: Vec<Vec<Box<str>>> = load_keys(&pnk)?
            .into_iter()
            .map(|name| {
                name.split('#')
                    .map(|name| name.to_string().into_boxed_str())
                    .collect()
            })
            .collect();
        let place_names = load_keys(&pnkp)?;

        let mut text_map = SortedTextMap::new(names);
        if let Ok(file) = File::open(story) {
            Self::load_story(&file, &mut text_map)?;
        }
        let SortedTextMap {
            names,
            mut contents,
        } = text_map;

        self.net_path = Some(folder_path);
        self.reset(net, names, place_names);

        let mut added_contents = HashMap::new();
        for (&tid, node) in &self.nodes.transitions.nodes {
            let mut path = node.path.clone();
            path.push(node.name.to_string().into_boxed_str());
            let Some(content) = contents.remove(&path) else {
                continue;
            };

            if !content.is_empty() {
                added_contents.insert(tid, content);
            }
        }
        for (tid, content) in added_contents {
            unsafe {
                self.nodes
                    .transitions
                    .nodes
                    .get_mut(&tid)
                    .unwrap_unchecked()
            }
            .content = TextContent::new(content);
        }

        if let Ok(mut file) = File::open(pnl) {
            self.load_layout(&mut file)?;
        }

        Ok(())
    }

    /// Loads a new net.
    pub fn load(&mut self, force_path: bool, ignore_save_warning: bool) {
        if ignore_save_warning || !self.ensure_saved(false) {
            return;
        }
        let path = if let (Some(path), false) = (&self.net_path, force_path) {
            path.clone()
        } else if let Some(path) = self.file_dialog().pick_folder() {
            path
        } else {
            return;
        };

        let _ = self.load_net(path);
    }
}
