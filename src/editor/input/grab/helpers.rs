use super::{Editor, snap};

use crate::{
    ActionButton,
    edit_list::NodeInstanceId,
    feature_types::{DuplicationType, Grab, GrabKind, MoveType, ViewMode},
    vector::Vector,
};

use bool_utils::BoolUtils;
use petri_net_simulation::{DynamicNetEdit, maximum_unfire_count};
use pns::Pid;

impl Editor {
    fn snapped_relative_mouse_pos(&self) -> Vector {
        let mut new_pos = self.relative_mouse_pos();
        if let Some(snap_size) = self.snapping {
            snap(&mut new_pos, snap_size);
        }
        new_pos
    }

    pub(super) fn extend_place_links(
        &mut self,
        pid: Pid,
        positions: &[Vector],
    ) -> impl Iterator<Item = NodeInstanceId> + use<> {
        self.changes = true;
        let node = unsafe { self.nodes.places.get_mut(&pid).unwrap_unchecked() };
        let old_len = node.positions.len();
        node.positions.extend(positions);
        (0..positions.len()).map(move |subindex| NodeInstanceId::Place(pid, subindex + old_len))
    }

    pub(super) fn grab_node(
        &mut self,
        node_id: NodeInstanceId,
        button: ActionButton,
        clear_old: bool,
        duplicate: bool,
    ) {
        self.changes = true;
        let grab_pos = self.relative_mouse_pos();
        let contains_node = self.selected.contains(&node_id);
        if clear_old && (!contains_node || button == ActionButton::Special) {
            self.selected.clear();
        }
        self.selected.add(node_id);
        let grab_kind = match button {
            ActionButton::Move => GrabKind::Move(MoveType::Nodes),
            ActionButton::Special => GrabKind::Connect,
        };
        let duplicate = (duplicate && button == ActionButton::Move)
            .then(|| clear_old.select(DuplicationType::New, DuplicationType::Linked));
        self.grab = Some(Grab {
            pos: grab_pos,
            kind: grab_kind,
            moved: false,
            deselect: (self.selected.is_single() || !clear_old) && contains_node,
            duplicate,
        });
    }

    pub(super) fn grab_empty(&mut self, button: ActionButton, clear_old: bool, create: bool) {
        let grab = if create {
            self.changes = true;
            if clear_old {
                self.selected.clear();
            }
            match button {
                ActionButton::Special => {
                    if self.view_mode == ViewMode::Actions {
                        return;
                    }
                    let pid = DynamicNetEdit::new(&mut self.net).add_place();
                    let place = self.add_place(pid, self.relative_mouse_pos(), String::new());
                    self.selected.add(place);
                }
                ActionButton::Move => {
                    if self.view_mode == ViewMode::State {
                        return;
                    }
                    let pos = self.relative_mouse_pos();
                    let [ox, oy] = self.node_settings.place_offset;
                    let offset = Vector::new(ox, oy);

                    let mut net = DynamicNetEdit::new(&mut self.net);
                    let ppid = net.add_place();
                    let npid = net.add_place();
                    let tid = net.add_connected_transition(&[ppid], &[npid]);
                    drop(net);

                    let new_node =
                        self.add_transition(tid, pos, self.transition_context.clone(), None);
                    self.selected.add(new_node);
                    let new_pplace = self.add_place(ppid, pos - offset, String::new());
                    self.selected.add(new_pplace);
                    let new_nplace = self.add_place(npid, pos + offset, String::new());
                    self.selected.add(new_nplace);
                }
            };
            Grab {
                pos: self.relative_mouse_pos(),
                kind: GrabKind::Move(MoveType::Nodes),
                moved: true,
                deselect: false,
                duplicate: None,
            }
        } else {
            Grab {
                pos: self.relative_mouse_pos(),
                kind: match button {
                    ActionButton::Move => GrabKind::Move(MoveType::View),
                    ActionButton::Special => GrabKind::Select,
                },
                moved: false,
                deselect: false,
                duplicate: None,
            }
        };

        self.grab = Some(grab);
    }

    fn connect_place(
        &mut self,
        connect_id: &NodeInstanceId,
        new_nodes: &mut Vec<NodeInstanceId>,
        allow_more: bool,
    ) -> bool {
        let mut new_transitions = Vec::new();
        let mut new_node = false;
        for (&pid, subindices) in self.selected.pids.iter() {
            use NodeInstanceId::*;
            match *connect_id {
                Transition(connect_tid) => {
                    let connected = DynamicNetEdit::new(&mut self.net)
                        .connect_place_to_transition(pid, connect_tid);
                    if connected || !allow_more {
                        continue;
                    }

                    if let Some(net) = self.net.default_mut() {
                        net.disconnect_place_to_transition(pid, connect_tid);
                    }
                }
                Place(connect_pid, subindex) => {
                    if pid == connect_pid && !allow_more {
                        new_node = true;
                        continue;
                    }

                    let node = &self.nodes.places[&pid];
                    let node_pos: Vector = subindices.iter().map(|&tid| node.positions[tid]).sum();
                    let node_pos = node_pos / subindices.len() as f32;
                    let other = &self.nodes.places[&connect_pid];
                    let other_pos = other.positions[subindex];
                    let new_pos = (node_pos + other_pos) / 2.0;
                    let new_tid = DynamicNetEdit::new(&mut self.net)
                        .add_connected_transition(&[pid], &[connect_pid]);
                    new_transitions.push((new_tid, new_pos));
                }
            }
        }
        for (new_tid, new_pos) in new_transitions {
            let transition =
                self.add_transition(new_tid, new_pos, self.transition_context.clone(), None);
            new_nodes.push(transition);
        }
        new_node
    }

    fn connect_transition(
        &mut self,
        connect_id: &NodeInstanceId,
        new_nodes: &mut Vec<NodeInstanceId>,
        allow_more: bool,
    ) -> bool {
        let mut new_places = Vec::new();
        let mut new_node = false;
        for &tid in &self.selected.tids {
            use NodeInstanceId::*;
            match *connect_id {
                Place(connect_pid, _subindex) => {
                    let connected = DynamicNetEdit::new(&mut self.net)
                        .connect_transition_to_place(tid, connect_pid);
                    if connected || !allow_more {
                        continue;
                    }

                    if let Some(net) = self.net.default_mut() {
                        net.disconnect_transition_to_place(tid, connect_pid);
                    }
                }
                Transition(connect_tid) => {
                    if tid == connect_tid && !allow_more {
                        new_node = true;
                        continue;
                    }

                    let node = &self.nodes.transitions.node(tid);
                    let other = &self.nodes.transitions.node(connect_tid);
                    let new_pos = (node.position + other.position) / 2.0;
                    if let Some(net) = self.net.default_mut() {
                        let new_pid = net.add_connected_place(&[tid], &[connect_tid]);
                        new_places.push((new_pid, new_pos));
                    }
                }
            }
        }
        for (new_pid, new_pos) in new_places {
            let place = self.add_linked_place(new_pid, &[new_pos], String::new());
            new_nodes.extend(place);
        }
        new_node
    }

    fn release_connect_more(&mut self, new_nodes: &mut Vec<NodeInstanceId>) {
        match self.view_mode {
            ViewMode::Default => {
                if !self.selected.pids.is_empty() {
                    let mut net = DynamicNetEdit::new(&mut self.net);
                    let in_pids: Vec<_> = self.selected.pids.keys().copied().collect();
                    let out_pid = net.add_place();
                    let connect_tid = net.add_connected_transition(&in_pids, &[out_pid]);
                    drop(net);

                    let pos = self.snapped_relative_mouse_pos();
                    let transition = self.add_transition(
                        connect_tid,
                        pos,
                        self.transition_context.clone(),
                        None,
                    );
                    new_nodes.push(transition);
                    let [ox, oy] = self.node_settings.place_offset;
                    let offset = Vector::new(ox, oy);
                    let place = self.add_linked_place(out_pid, &[pos + offset], String::new());
                    new_nodes.extend(place);
                }
                if !self.selected.tids.is_empty() {
                    let Some(minimal_token_count) = self.net.simulated().map_or(Some(0), |net| {
                        self.selected.tids.iter().try_fold(0, |result, &tid| {
                            maximum_unfire_count(net, tid).map(|count| result.max(count))
                        })
                    }) else {
                        return;
                    };

                    let mut net = DynamicNetEdit::new(&mut self.net);
                    let connect_pid = net.add_place();
                    net.add_initial_tokens(connect_pid, minimal_token_count);
                    for &tid in &self.selected.tids {
                        net.connect_transition_to_place(tid, connect_pid);
                    }
                    drop(net);

                    let place = self.add_place(
                        connect_pid,
                        self.snapped_relative_mouse_pos(),
                        String::new(),
                    );
                    new_nodes.push(place);
                }
            }
            ViewMode::State => {
                let connect_pid = DynamicNetEdit::new(&mut self.net).add_place();
                let new_pos = self.snapped_relative_mouse_pos();
                let place = self.add_place(connect_pid, new_pos, String::new());
                new_nodes.push(place);
                let selected_pids: Vec<_> = self.selected.pids.keys().copied().collect();
                for pid in selected_pids {
                    let node = &self.nodes.places[&pid];
                    let Some((pos, _)) = node.positions.iter().fold(None, |result, pos| {
                        let new_distance = (pos - self.relative_mouse_pos()).norm_squared();
                        if let Some((position, distance)) = result {
                            if new_distance >= distance {
                                return Some((position, distance));
                            }
                        }
                        Some((*pos, new_distance))
                    }) else {
                        continue;
                    };

                    let new_tid = DynamicNetEdit::new(&mut self.net)
                        .add_connected_transition(&[pid], &[connect_pid]);
                    let transition = self.add_transition(
                        new_tid,
                        (pos + new_pos) / 2.0,
                        self.transition_context.clone(),
                        None,
                    );
                    new_nodes.push(transition);
                }
            }
            ViewMode::Actions => {
                let mut new_pids = Vec::new();
                let selected_tids: Vec<_> = self.selected.tids.iter().copied().collect();
                let new_pos = self.snapped_relative_mouse_pos();
                for tid in selected_tids {
                    let mut net = DynamicNetEdit::new(&mut self.net);
                    let new_pid = net.add_place();
                    net.connect_transition_to_place(tid, new_pid);
                    drop(net);

                    let node_pos = self.nodes.transitions.node(tid).position;
                    let place = self.add_place(new_pid, (node_pos + new_pos) / 2.0, String::new());
                    new_nodes.push(place);
                    new_pids.push(new_pid);
                }
                let connect_tid =
                    DynamicNetEdit::new(&mut self.net).add_connected_transition(&new_pids, &[]);
                let transition = self.add_transition(
                    connect_tid,
                    new_pos,
                    self.transition_context.clone(),
                    None,
                );
                new_nodes.push(transition);
            }
        }
    }

    pub(super) fn release_connect(&mut self, allow_more: bool, clear_old: bool, select_new: bool) {
        let mut new_nodes = Vec::new();
        if let Some(connect_node) = self.click_info(self.relative_mouse_pos()) {
            if self.connect_place(&connect_node, &mut new_nodes, allow_more)
                || self.connect_transition(&connect_node, &mut new_nodes, allow_more)
            {
                new_nodes.push(connect_node);
            }
        } else if allow_more {
            self.release_connect_more(&mut new_nodes);
        }
        if clear_old {
            self.selected.clear();
        }
        if select_new {
            for node in new_nodes {
                self.selected.add(node);
            }
        }
    }
}
