use super::Editor;

use crate::{
    CursorBoundary, LineDirection, LineScope, feature_types::SearchId, text_content::TextCursor,
};

use single_whitespace_text_editing::SingleWhitespaceEditAction;
use text_editing::{Direction, TextLine};

impl Editor {
    /// Input text.
    pub fn text_input(&mut self, character: char, skip: bool) {
        if character == '#' {
            return;
        }
        let Some(edit_action) = SingleWhitespaceEditAction::new(character, skip) else {
            return;
        };

        if let Some(string_searcher) = &mut self.string_searcher {
            if !edit_action.apply(&mut string_searcher.name, &mut string_searcher.name_cursor) {
                return;
            }

            let subtext = &string_searcher.name;
            if subtext.is_empty() {
                string_searcher.list.pids.clear();
                string_searcher.list.tids.clear();
                string_searcher.index = None;
                return;
            }

            string_searcher.list.pids = self
                .nodes
                .places
                .iter()
                .filter_map(|(&pid, node)| {
                    let node_name = node.name.as_str();
                    let subtext = subtext.as_str();
                    if node_name.contains(subtext) {
                        return Some(pid);
                    }
                    None
                })
                .collect();
            string_searcher.list.tids = self
                .nodes
                .transitions
                .nodes
                .iter()
                .filter_map(|(&tid, node)| {
                    let node_name = node.name.as_str();
                    let subtext = subtext.as_str();
                    if node_name.contains(subtext) {
                        return Some(tid);
                    }
                    None
                })
                .collect();
            match (
                string_searcher.list.pids.is_empty(),
                string_searcher.list.tids.is_empty(),
            ) {
                (true, true) => string_searcher.index = None,
                (true, false) => string_searcher.index = Some(SearchId::Transition(0)),
                (false, true) => string_searcher.index = Some(SearchId::Place(0)),
                (false, false) => {
                    let Some(index) = &mut string_searcher.index else {
                        string_searcher.index = Some(SearchId::Transition(0));
                        return;
                    };

                    use SearchId::*;
                    *index = match index {
                        Place(_) => Place(0),
                        Transition(_) => Transition(0),
                    };
                }
            }

            return;
        }

        if self.text_editor_visible() {
            self.changes = true;
            let content = &mut self
                .nodes
                .transitions
                .node_mut(*unsafe { self.selected.tids.iter().next().unwrap_unchecked() })
                .content;

            if content.cursor.column == 0 && character == '\x08' && content.cursor.row > 0 {
                let line = content.lines.remove(content.cursor.row);
                content.cursor.row -= 1;
                if content.offset > 0 {
                    content.offset -= 1;
                }
                content.cursor.column = content.lines[content.cursor.row].len();
                content.lines[content.cursor.row].join(line);

                return;
            }

            if content.cursor.column == content.lines[content.cursor.row].len()
                && character == '\x7F'
                && content.cursor.row < content.lines.len() - 1
            {
                let line = content.lines.remove(content.cursor.row + 1);
                content.lines[content.cursor.row].join(line);
                return;
            }

            edit_action.apply(
                &mut content.lines[content.cursor.row],
                &mut content.cursor.column,
            );

            return;
        }

        self.changes = true;
        for &tid in &self.selected.tids {
            let node = self.nodes.transitions.node_mut(tid);
            edit_action.apply(&mut node.name, &mut node.name_cursor);
        }
        for pid in self.selected.pids.keys() {
            let node = unsafe { self.nodes.places.get_mut(pid).unwrap_unchecked() };
            edit_action.apply(&mut node.name, &mut node.name_cursor);
        }
    }

    /// Finish the current action.
    /// Basically the user presses the enter key.
    pub fn finish(&mut self, clear_old: bool, skip: bool) {
        if self.text_editor_visible() {
            self.changes = true;
            let line_count = self.line_count();
            let content = &mut self
                .nodes
                .transitions
                .node_mut(*unsafe { self.selected.tids.iter().next().unwrap_unchecked() })
                .content;
            let line = content.lines[content.cursor.row].split(content.cursor.column);
            content.cursor.row += 1;
            if content.cursor.row >= content.offset + line_count {
                content.offset = content.cursor.row - line_count;
            }
            content.cursor.column = 0;
            content.lines.insert(content.cursor.row, line);

            return;
        }

        let Some(string_searcher) = &mut self.string_searcher else {
            return;
        };
        if clear_old {
            self.selected.clear();
        }
        if skip {
            for &pid in &string_searcher.list.pids {
                if !self.selected.pids.contains_key(&pid) {
                    self.selected.pids.insert(pid, 0);
                }
            }
            for &tid in &string_searcher.list.tids {
                self.selected.tids.insert(tid);
            }
        } else if let Some(index) = &string_searcher.index {
            use SearchId::*;
            match *index {
                Place(pindex) => {
                    let pid = *unsafe {
                        string_searcher
                            .list
                            .pids
                            .iter()
                            .nth(pindex)
                            .unwrap_unchecked()
                    };
                    if !self.selected.pids.contains_key(&pid) {
                        self.selected.pids.insert(pid, 0);
                    }
                }
                Transition(tindex) => {
                    let tid = *unsafe {
                        string_searcher
                            .list
                            .tids
                            .iter()
                            .nth(tindex)
                            .unwrap_unchecked()
                    };
                    self.selected.tids.insert(tid);
                }
            }
        }

        self.string_searcher = None;
    }

    /// Escapes from submenus.
    pub fn escape(&mut self) {
        self.string_searcher = None;
        self.text_content_shown = false;
    }

    /// Move the text cursor in line.
    pub fn move_text_cursor(&mut self, direction: Direction, skip: bool) {
        let call = TextLine::cursor_movement(direction, skip);

        if self.text_editor_visible() {
            let content = &mut self
                .nodes
                .transitions
                .node_mut(*unsafe { self.selected.tids.iter().next().unwrap_unchecked() })
                .content;
            let lines = &content.lines;
            let cursor = &mut content.cursor;
            let cursor_call = TextCursor::line_changer(direction);

            if skip {
                while !call(&lines[cursor.row], &mut cursor.column) {
                    if !cursor_call(cursor, lines) {
                        break;
                    }
                }
            } else if !call(&lines[cursor.row], &mut cursor.column) {
                cursor_call(cursor, lines);
            }
        } else if let Some(string_searcher) = &mut self.string_searcher {
            call(&string_searcher.name, &mut string_searcher.name_cursor);
        } else {
            for pid in self.selected.pids.keys() {
                let node = unsafe { self.nodes.places.get_mut(pid).unwrap_unchecked() };
                call(&node.name, &mut node.name_cursor);
            }
            for &tid in &self.selected.tids {
                let node = self.nodes.transitions.node_mut(tid);
                call(&node.name, &mut node.name_cursor);
            }
        }
    }

    /// Moves the text cursor by one line in the specified direction.
    pub fn move_line(&mut self, direction: LineDirection) {
        if !self.text_editor_visible() {
            return;
        }

        let line_count = self.line_count();
        let content = &mut self
            .nodes
            .transitions
            .node_mut(*unsafe { self.selected.tids.iter().next().unwrap_unchecked() })
            .content;

        match direction {
            LineDirection::Down => {
                if content.cursor.row >= content.lines.len() - 1 {
                    content.cursor.column = content.lines[content.cursor.row].len();
                    return;
                }

                content.cursor.row += 1;
                if content.cursor.column > content.lines[content.cursor.row].len() {
                    content.cursor.column = content.lines[content.cursor.row].len();
                }
                if content.cursor.row >= content.offset + line_count {
                    content.offset = content.cursor.row - line_count;
                }
            }
            LineDirection::Up => {
                if content.cursor.row == 0 {
                    content.cursor.column = 0;
                    return;
                }

                content.cursor.row -= 1;
                if content.cursor.column > content.lines[content.cursor.row].len() {
                    content.cursor.column = content.lines[content.cursor.row].len();
                }
                if content.cursor.row < content.offset {
                    content.offset = content.cursor.row;
                }
            }
        }
    }

    /// Moves the text by one page in the specified direction.
    pub fn move_page(&mut self, direction: LineDirection) {
        if !self.text_editor_visible() {
            return;
        }

        let line_count = self.line_count();
        let content = &mut self
            .nodes
            .transitions
            .node_mut(*unsafe { self.selected.tids.iter().next().unwrap_unchecked() })
            .content;

        match direction {
            LineDirection::Down => {
                if content.offset + line_count * 2 < content.lines.len() - 1 {
                    content.offset += line_count;
                } else if content.offset + line_count < content.lines.len() - 1 {
                    content.offset = content.lines.len() - 1 - line_count;
                }

                if content.cursor.row + line_count < content.lines.len() - 1 {
                    content.cursor.row += line_count;
                } else {
                    content.cursor.row = content.lines.len() - 1;
                    content.cursor.column = content.lines[content.cursor.row].len();
                }
            }
            LineDirection::Up => {
                if content.offset > line_count {
                    content.offset -= line_count;
                } else {
                    content.offset = 0;
                }
                if content.cursor.row > line_count {
                    content.cursor.row -= line_count;
                } else {
                    content.cursor.row = 0;
                    content.cursor.column = 0;
                }
            }
        }

        if content.lines[content.cursor.row].len() < content.cursor.column {
            content.cursor.column = content.lines[content.cursor.row].len();
        }
    }

    fn move_text_cursor_start(&mut self, scope: LineScope) {
        if self.text_editor_visible() {
            let content = &mut self
                .nodes
                .transitions
                .node_mut(*unsafe { self.selected.tids.iter().next().unwrap_unchecked() })
                .content;

            if scope == LineScope::AllLines {
                content.offset = 0;
                content.cursor.row = 0;
            }
            content.cursor.column = 0;
        } else if let Some(string_searcher) = &mut self.string_searcher {
            string_searcher.name_cursor = 0
        } else {
            for pid in self.selected.pids.keys() {
                unsafe { self.nodes.places.get_mut(pid).unwrap_unchecked() }.name_cursor = 0;
            }
            for &tid in &self.selected.tids {
                self.nodes.transitions.node_mut(tid).name_cursor = 0;
            }
        }
    }

    fn move_text_cursor_end(&mut self, scope: LineScope) {
        if self.text_editor_visible() {
            let line_count = self.line_count();
            let content = &mut self
                .nodes
                .transitions
                .node_mut(*unsafe { self.selected.tids.iter().next().unwrap_unchecked() })
                .content;

            if scope == LineScope::AllLines {
                if content.lines.len() > line_count {
                    content.offset = content.lines.len() - 1 - line_count;
                }
                content.cursor.row = content.lines.len() - 1;
            }
            content.cursor.column = content.lines[content.cursor.row].len();
        } else if let Some(string_searcher) = &mut self.string_searcher {
            string_searcher.name_cursor = string_searcher.name.len()
        } else {
            for pid in self.selected.pids.keys() {
                let node = unsafe { self.nodes.places.get_mut(pid).unwrap_unchecked() };
                node.name_cursor = node.name.len();
            }
            for &tid in &self.selected.tids {
                let node = self.nodes.transitions.node_mut(tid);
                node.name_cursor = node.name.len();
            }
        }
    }

    /// Moves the text cursor to the start or end of a line or file.
    pub fn move_text_cursor_border(&mut self, boundary: CursorBoundary, scope: LineScope) {
        match boundary {
            CursorBoundary::Start => self.move_text_cursor_start(scope),
            CursorBoundary::End => self.move_text_cursor_end(scope),
        }
    }
}
