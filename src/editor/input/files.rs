mod load;
mod save;

use super::{Editor, snap};

use crate::state_info::{CallableState, StateInfo};

use rfd::{FileDialog, MessageButtons, MessageDialog, MessageLevel};

use event_simulation::Simulation;

use std::{
    fs::File,
    io::{Read, Write},
    path::{Path, PathBuf},
};

impl Editor {
    fn file_dialog(&self) -> FileDialog {
        let dialog = FileDialog::new();

        if let Some(path) = &self.net_path {
            dialog.set_directory(path)
        } else if let Ok(path) = std::env::current_dir() {
            dialog.set_directory(path)
        } else {
            dialog
        }
    }

    fn project_paths(folder_path: &Path) -> Option<(PathBuf, PathBuf, PathBuf, PathBuf, PathBuf)> {
        let Some(folder_name) = folder_path.file_stem() else {
            MessageDialog::new()
                .set_level(MessageLevel::Error)
                .set_title("Error!")
                .set_description("Invalid project directory name")
                .set_buttons(MessageButtons::Ok)
                .show();
            return None;
        };

        let mut path = folder_path.to_path_buf();
        path.push(folder_name);

        Some((
            path.with_extension("pn"),
            path.with_extension("pnl"),
            path.with_extension("pnk"),
            path.with_extension("pnkp"),
            path.with_extension("story"),
        ))
    }

    /// Saves the current progress of a simulation.
    pub fn save_progress(&mut self, force_path: bool) {
        let Some(net) = self.net.simulated_mut() else {
            return;
        };

        let info = &mut self.state_infos[self.simulation];
        if info.state_path.is_none() || force_path {
            let Some(path) = info.file_dialog().save_file() else {
                return;
            };

            info.state_path = Some(path);
        }

        fn save(data: &[usize], filename: &Path) -> std::io::Result<()> {
            let mut file = File::create(filename)?;
            for &count in data {
                file.write_all(&(count as u32).to_le_bytes())?;
            }

            Ok(())
        }

        let simulation = unsafe { net.get_mut(self.simulation).unwrap_unchecked() };

        if let Err(err) = save(simulation.data(), unsafe {
            info.state_path.as_ref().unwrap_unchecked()
        }) {
            MessageDialog::new()
                .set_level(MessageLevel::Error)
                .set_title("Saving simulation failed!")
                .set_description(format!("Error: {err:?}"))
                .set_buttons(MessageButtons::Ok)
                .show();

            return;
        }

        info.changes = false;
    }

    /// Loads the some progress for the simulation.
    pub fn load_progress(&mut self) {
        if self.net.simulated_mut().is_none() {
            return;
        }

        let info = &self.state_infos[self.simulation];
        let Some(path) = info.file_dialog().pick_file() else {
            return;
        };

        let Some(net) = self.net.simulated_mut() else {
            return;
        };

        fn read_values(filename: &Path) -> std::io::Result<Vec<u32>> {
            let size = (std::fs::metadata(filename)?.len() + 3) / 4;

            let mut file = File::open(filename)?;

            (0..size)
                .map(|_| {
                    let mut value = [0; 4];
                    file.read_exact(&mut value)?;
                    Ok(u32::from_le_bytes(value))
                })
                .collect()
        }

        let Ok(values) = read_values(&path) else {
            return;
        };

        if net.add_simulation_from_data(values).is_err() {
            return;
        };

        let new_index = net.states().len() - 1;
        let mut info = StateInfo {
            state_path: Some(path),
            changes: false,
            callables: net
                .transition_ids()
                .map(|tid| (tid, CallableState::default()))
                .collect(),
        };
        self.simulation = new_index;
        info.update_state(&mut net.states_mut()[new_index]);
        self.state_infos.push(info);
    }
}
