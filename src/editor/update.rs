use super::Editor;

use crate::feature_types::{Grab, GrabKind, MoveType, SearchId};

use gapp::Update;

impl Editor {
    fn update_string_searcher(&mut self) {
        let Some(string_searcher) = &self.string_searcher else {
            return;
        };

        let Some(index) = &string_searcher.index else {
            return;
        };

        use SearchId::*;
        if let Some(goal_pos) = match *index {
            Place(pindex) => {
                let pid = *unsafe {
                    string_searcher
                        .list
                        .pids
                        .iter()
                        .nth(pindex)
                        .unwrap_unchecked()
                };
                let node = &self.nodes.places[&pid];
                node.positions.iter().fold(None, |result, pos| {
                    let new_distance = (pos - self.view_offset).norm_squared();
                    if let Some((position, distance)) = result {
                        if new_distance >= distance {
                            return Some((position, distance));
                        }
                    }
                    Some((pos, new_distance))
                })
            }
            Transition(tindex) => {
                let tid = *unsafe {
                    string_searcher
                        .list
                        .tids
                        .iter()
                        .nth(tindex)
                        .unwrap_unchecked()
                };
                Some((&self.nodes.transitions.node(tid).position, 0.0))
            }
        } {
            self.view_offset += (goal_pos.0 - self.view_offset) / 4.0;
        }
    }
}

impl Update for Editor {
    fn update(&mut self, _timestep: f32) {
        if let Some(Grab {
            kind: GrabKind::Move(MoveType::Nodes) | GrabKind::Connect | GrabKind::Select,
            moved: true,
            ..
        }) = self.grab
        {
            const WS: f32 = 6.0;
            let movefac = self.zoom * 0x400 as f32;
            let window_left = (self.mouse_pos.x - self.window_size.x / WS) / self.window_size.x;
            if window_left < 0.0 {
                self.view_offset.x -= window_left.powi(2) * movefac;
            }
            let window_right = (1.0 - self.mouse_pos.x + self.window_size.x * (WS - 1.0) / WS)
                / self.window_size.x;
            if window_right < 0.0 {
                self.view_offset.x += window_right.powi(2) * movefac;
            }
            let window_up = (self.mouse_pos.y - self.window_size.y / WS) / self.window_size.y;
            if window_up < 0.0 {
                self.view_offset.y -= window_up.powi(2) * movefac;
            }
            let window_down = (1.0 - self.mouse_pos.y + self.window_size.y * (WS - 1.0) / WS)
                / self.window_size.y;
            if window_down < 0.0 {
                self.view_offset.y += window_down.powi(2) * movefac;
            }
        }

        let relative_mouse_pos = self.relative_mouse_pos();
        if let Some(Grab {
            pos: grab_pos,
            kind: GrabKind::Move(move_type),
            ..
        }) = &mut self.grab
        {
            let movevec = relative_mouse_pos - *grab_pos;

            match move_type {
                MoveType::Nodes => {
                    for (pid, subindices) in self.selected.pids.iter() {
                        let positions =
                            &mut unsafe { self.nodes.places.get_mut(pid).unwrap_unchecked() }
                                .positions;
                        for &subindex in subindices {
                            positions[subindex] += movevec;
                        }
                    }
                    for &tid in &self.selected.tids {
                        self.nodes.transitions.node_mut(tid).position += movevec;
                    }
                    *grab_pos += movevec;
                }
                MoveType::View => self.view_offset -= movevec,
            }
        }

        self.update_string_searcher();
    }
}
