use std::collections::{BTreeMap, BTreeSet};

pub struct PathMap<V> {
    pub values: BTreeSet<V>,
    pub children: BTreeMap<Box<str>, PathMap<V>>,
}

impl<V: Ord> PathMap<V> {
    pub fn new() -> Self {
        Self {
            values: BTreeSet::new(),
            children: BTreeMap::new(),
        }
    }

    pub fn insert(&mut self, mut path: Vec<Box<str>>, value: V) -> Option<V> {
        path.reverse();
        self.insert_rev(path, value)
    }

    fn insert_rev(&mut self, mut path: Vec<Box<str>>, value: V) -> Option<V> {
        let Some(key) = path.pop() else {
            return self.values.replace(value);
        };

        if let Some(child) = self.children.get_mut(&key) {
            return child.insert_rev(path, value);
        }

        let mut child = Self::new();
        child.insert_rev(path, value);
        self.children.insert(key, child);

        None
    }

    pub fn remove(&mut self, path: &[Box<str>], value: &V) -> Option<V> {
        if path.is_empty() {
            return self.values.take(value);
        }

        let key = &path[0];
        self.children
            .get_mut(key)
            .and_then(|child| child.remove(&path[1..], value))
    }
}
