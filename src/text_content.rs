use text_editing::{Direction, TextLine};

pub struct TextCursor {
    pub row: usize,
    pub column: usize,
}

impl TextCursor {
    pub fn new() -> Self {
        Self { row: 0, column: 0 }
    }

    pub fn line_changer(direction: Direction) -> fn(&mut Self, &[TextLine]) -> bool {
        use Direction::*;
        match direction {
            Forward => Self::next_line,
            Backward => Self::prev_line,
        }
    }

    pub fn next_line(&mut self, lines: &[TextLine]) -> bool {
        if self.row == lines.len() - 1 {
            return false;
        }

        self.row += 1;
        self.column = 0;

        true
    }

    pub fn prev_line(&mut self, lines: &[TextLine]) -> bool {
        if self.row == 0 {
            return false;
        }

        self.row -= 1;
        self.column = lines[self.row].len();

        true
    }
}

pub struct TextContent {
    pub lines: Vec<TextLine>,
    pub cursor: TextCursor,
    pub offset: usize,
}

impl TextContent {
    pub fn new(lines: Vec<TextLine>) -> Self {
        Self {
            lines,
            cursor: TextCursor::new(),
            offset: 0,
        }
    }
}
