use crate::{
    edit_list::SelectionList,
    feature_types::{Grab, StringSearcher, ViewMode},
    node::NodeContainer,
    node_settings::NodeSettings,
    state_info::StateInfo,
    vector::Vector,
};

use copypasta::ClipboardContext;
use petri_net_simulation::{DynamicNet, MultiPetriNetSimulation};
use pns::Net;

use std::path::PathBuf;

/// This represents the editor you want to use to edit and display petri nets.
pub struct Editor {
    net: DynamicNet<MultiPetriNetSimulation>,
    text_content_shown: bool,
    nodes: NodeContainer,
    transition_context: Vec<Box<str>>,
    window_size: Vector,
    zoom: f32,
    selected: SelectionList,
    grab: Option<Grab>,
    view_mode: ViewMode,
    net_path: Option<PathBuf>,
    state_infos: Vec<StateInfo>,
    simulation: usize,
    mouse_pos: Vector,
    view_offset: Vector,
    clipboard: Option<ClipboardContext>,
    string_searcher: Option<StringSearcher>,
    changes: bool,
    snapping: Option<f32>,
    node_settings: NodeSettings,
}

impl Editor {
    /// Creates a new editor, which contains a renderer and knows about the window size.
    pub fn new(width: f32, height: f32, node_settings: NodeSettings) -> Self {
        Self {
            net: DynamicNet::Default(Net::new().into()),
            text_content_shown: false,
            nodes: NodeContainer::empty(),
            transition_context: Vec::new(),
            window_size: Vector::new(width, height),
            zoom: 1.0,
            selected: SelectionList::new(),
            grab: None,
            view_mode: ViewMode::Default,
            net_path: None,
            state_infos: Vec::new(),
            simulation: 0,
            mouse_pos: Vector::new(0.0, 0.0),
            view_offset: Vector::new(0.0, 0.0),
            string_searcher: None,
            clipboard: ClipboardContext::new().ok(),
            changes: false,
            snapping: Some(16.0),
            node_settings,
        }
    }

    fn relative_mouse_pos(&self) -> Vector {
        (self.mouse_pos - self.window_size / 2.0) * self.zoom + self.view_offset
    }

    fn text_editor_visible(&self) -> bool {
        self.text_content_shown && self.selected.tids.len() == 1
    }

    /// Indicates if snapping is enabled.
    pub fn snapping(&self) -> bool {
        self.snapping.is_some()
    }

    /// Indicates if the text editor can be shown.
    pub fn text_editor_shown(&self) -> bool {
        self.text_content_shown
    }

    /// Indicates if the simuliton mode is active.
    pub fn simulation_mode(&self) -> bool {
        self.net.simulated().is_some()
    }

    /// Indicates if the search mode is active.
    pub fn search_mode(&self) -> bool {
        self.string_searcher.is_some()
    }

    /// Access the current view mode.
    pub fn view_mode(&self) -> ViewMode {
        self.view_mode
    }
}

mod input;
mod render;
mod update;
