/// Different buttons are used for different actions.
#[derive(PartialEq, Eq, Copy, Clone)]
pub enum ActionButton {
    /// Used for movement related actions.
    Move,
    /// Used for connection and selection actions.
    Special,
}

/// The fire direction for firing transitions.
#[derive(PartialEq, Eq, Copy, Clone)]
pub enum FireDirection {
    /// Fire forward.
    Forward,
    /// Fire backward.
    Backward,
}

/// Represents the vertical direction for moving the text cursor.
#[derive(PartialEq, Eq, Copy, Clone)]
pub enum LineDirection {
    /// Move the text cursor up by one line.
    Up,
    /// Move the text cursor down by one line.
    Down,
}

/// Represents the cursor position at the start or end of a line or file.
#[derive(PartialEq, Eq, Copy, Clone)]
pub enum CursorBoundary {
    /// Move the cursor to the start of the line or file.
    Start,
    /// Move the cursor to the end of the line or file.
    End,
}

/// Represents the scope of lines affected by the cursor movement.
#[derive(PartialEq, Eq, Copy, Clone)]
pub enum LineScope {
    /// Move the cursor within the current line.
    CurrentLine,
    /// Move the cursor to the start of the first line or end of the last line.
    AllLines,
}
