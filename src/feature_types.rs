use crate::{edit_list::EditList, vector::Vector};

use text_editing::TextLine;

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum MoveType {
    Nodes,
    View,
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum GrabKind {
    Move(MoveType),
    Connect,
    Select,
}

#[derive(PartialEq, Eq, Copy, Clone)]
pub enum DuplicationType {
    New,
    Linked,
}

#[derive(PartialEq)]
pub struct Grab {
    pub kind: GrabKind,
    pub pos: Vector,
    pub moved: bool,
    pub deselect: bool,
    pub duplicate: Option<DuplicationType>,
}

/// The view mode indicates how the petri net is displayed.
#[derive(Copy, Clone, PartialEq, Eq)]
pub enum ViewMode {
    /// In default mode, everything is displayed.
    Default,
    /// In state mode, only places are displayed.
    /// Working in state mode is like working with a state machine.
    State,
    /// In action mode, only transitions are displayed.
    /// Working in action mode is like working with a dependency graph.
    Actions,
}

pub enum SearchId {
    Transition(usize),
    Place(usize),
}

pub struct StringSearcher {
    pub name: TextLine,
    pub name_cursor: usize,
    pub index: Option<SearchId>,
    pub list: EditList,
}
