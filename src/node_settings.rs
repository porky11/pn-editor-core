/// Settings for the sizes of the nodes.
pub struct NodeSettings {
    /// The radius used for drawing places.
    pub radius: f32,
    /// The width used for drawing transitions.
    pub width: f32,
    /// The height used for drawing transitions.
    pub height: f32,
    /// The expansion of a transition frame.
    pub expand: f32,
    /// The offset when of automatically created places.
    pub place_offset: [f32; 2],
}

impl Default for NodeSettings {
    fn default() -> Self {
        Self {
            radius: 0x10 as f32,
            height: 0x20 as f32,
            width: 0x80 as f32,
            expand: 0x20 as f32,
            place_offset: [0x60 as f32, 0.0],
        }
    }
}
