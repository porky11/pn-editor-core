use pns::{FireChanges, State, Tid};
use rfd::FileDialog;

use std::{collections::BTreeMap, path::PathBuf};

/// Indicates if some transition is callable.
#[derive(Default, Copy, Clone)]
pub struct CallableState {
    /// The transition is callable forward.
    pub forward: bool,
    /// The transition is callable backward.
    pub backward: bool,
}

pub struct StateInfo {
    pub state_path: Option<PathBuf>,
    pub changes: bool,
    pub callables: BTreeMap<Tid, CallableState>,
}

impl StateInfo {
    pub fn update_state(&mut self, state: &mut State) {
        let FireChanges { added, removed } = state.changed_transitions();

        for added in added {
            unsafe { self.callables.get_mut(&added).unwrap_unchecked() }.forward = true;
        }
        for removed in removed {
            unsafe { self.callables.get_mut(&removed).unwrap_unchecked() }.forward = false;
        }

        let FireChanges { added, removed } = state.changed_transitions_backward();

        for added in added {
            unsafe { self.callables.get_mut(&added).unwrap_unchecked() }.backward = true;
        }
        for removed in removed {
            unsafe { self.callables.get_mut(&removed).unwrap_unchecked() }.backward = false;
        }
    }

    pub fn file_dialog(&self) -> FileDialog {
        let dialog = FileDialog::new().add_filter("Petri net interactive files", &["pns"]);

        if let Some(path) = &self.state_path {
            dialog.set_directory(path)
        } else if let Ok(path) = std::env::current_dir() {
            dialog.set_directory(path)
        } else {
            dialog
        }
    }
}
