#![deny(missing_docs)]

/*!
This contains the logic for editing petri nets.
In order to use it, you need to implement the `Renderer` trait for your renderer.
Then you can create a new `Editor`.
Then you need to call the main events `input`, `render` and `update` repeatedly as specified.
In order to call `input`, you need to create an `InputEvent`.
**/

mod edit_list;
mod editor;
mod feature_types;
mod input_types;
mod node;
mod node_settings;
mod path_map;
mod renderer;
mod state_info;
mod text_content;
mod vector;

pub use editor::Editor;
pub use feature_types::ViewMode;
pub use input_types::{ActionButton, CursorBoundary, FireDirection, LineDirection, LineScope};
pub use node_settings::NodeSettings;
pub use renderer::{ArrowKind, Renderer};
pub use state_info::CallableState;
